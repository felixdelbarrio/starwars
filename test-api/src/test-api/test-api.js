import {LitElement, html} from 'lit-element'


class TestApi extends LitElement {

    static get properties() {
        return{
            movies: {type: Array}
        }
    }

    constructor() {
        super()

        this.movies = []
        this.getMovieData()

    }
    render(){
        return html`
             ${this.movies.map(
                movie => html `<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}</div>`
            )} 
        `
    }

    getMovieData() {
        console.log('getMovieData')
        console.log('Obteniendo datos de las peliculas')

        let xhr = new XMLHttpRequest()

        xhr.onload = ()=> {
            if (xhr.status === 200) {
                console.log("Peticion se ha completado correctamente")

                let APIResponse = JSON.parse(xhr.responseText)
                console.log(APIResponse)

                //Ojo que aqui tendremos que ver el nodo del JSON que queremos consultar en la docum de la API
                //Este es un fallo común que se da en el hackaton
                this.movies = APIResponse.results


            }
        }

        xhr.open('GET', 'https://swapi.dev/api/films/')
        //Para un POST en el hr.send('BODY REQUEST')
        xhr.send()

        console.log('Fin de getMovieData')

    }

}

customElements.define('test-api', TestApi)